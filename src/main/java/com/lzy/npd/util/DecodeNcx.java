package com.lzy.npd.util;

import com.lzy.npd.enums.VersionEnum;
import com.lzy.npd.factory.NavicatCipherFactory;
import com.lzy.npd.navicat.NavicatChiper;

/**
 * DecodeNcx 解密navicat导出的密码
 *
 * <p>
 * navicat 11采用BF(blowfish)-ECB方式，对应mode为ECB
 * navicat 12以上采用AES-128-CBC方式，对应mode为CBC
 *
 * @author lzy
 * @date 2022/01/10 15:13
 */
public class DecodeNcx {

    private static String mode;

    public DecodeNcx(String mode) {
        DecodeNcx.mode = mode;
    }

    /**
     * 根据mode进行解密
     *
     * @param str 密文
     * @return String
     */
    public String decode(String str) {
        if (StringUtil.isEmpty(str)) {
            return "";
        }
        NavicatChiper chiper = NavicatCipherFactory.get(mode);
        return chiper.decryptString(str);
    }

    /**
     * 如何获取密码密文？
     * @link <a href="https://blog.csdn.net/kkk123445/article/details/122514124"/>
     **/
    public static void main(String[] args) {
        //navicat11
        DecodeNcx decodeNcx = new DecodeNcx(VersionEnum.native11.name());
        System.out.println(decodeNcx.decode("15057D7BA390"));
        //navicat12+
        DecodeNcx decodeNcxMore = new DecodeNcx(VersionEnum.navicat12more.name());
        System.out.println(decodeNcxMore.decode("503AA930968F877F04770B47DD731DC0"));
    }
}
